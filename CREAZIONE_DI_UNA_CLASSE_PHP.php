<!--Questo programma inserisce in un un oggetto in php i dati di un form compilato dall'utente -->
<?php
//creo la classe Persona che sarà la classe padre
class Persona{
//dichiaro 3 variabili
    private $nome;
    private $cognome;
    private $eta;
//con il metodo construct creo il costruttore
//che dice cosa fare al momento della creazione dell'oggetto
// __ indicano funzioni magiche riservate a php
    protected function __construct($nome, $cognome, $eta) {
 //assegnamo il valore del parametro nome alla proprietà nome dell'oggetto tramite
 // il puntatore this dell'oggetto stesso
        $this->nome=$nome;
        $this->cognome=$cognome;
        $this->eta=$eta;
    }
//creo il metodo getStudente che è protetto, ma può essere ereditato dalle classi figlie
//preleva e stampa le proprietà dell'oggetto stesso quando viene chiamato
    protected function getPersona(){

        echo "nome : ". $this->nome."<br>";
        echo "cognome : ". $this->cognome."<br>";
        echo "eta : ". $this->eta."<br>";
//assegno alla variabile $Risultato le proprietà
        $Risultato= $this->nome."\n".$this->cognome."\n".$this->eta;
        return $Risultato;
    }
}
//creo la classe Studente e con extends la faccio ereditare al padre
class Studente extends Persona {
    public $universita;
//creo il costruttore e come parametri gli passo quello che servirà per costruire l'oggetto
    public function __construct($nome, $cognome, $eta, $universita){
//chiamo il costruttore del padre con parent::(classe persona)
        parent::__construct($nome, $cognome, $eta);
//accedo alla proprietà dell'oggetto tramite il puntatore this e gli assegno la variabile $università
        $this->universita=$universita;
    }
//creo il metodo getStudente
    public function getStudente(){
//assegno il risultato del metodo getPersona del padre alla variabile $Dati
        $Dati = parent::getPersona();
        echo "universita : ". $this->universita."<br>";
//assegno alla variabile $DatiCompleti la concatenazione  dei dati nome cognome e  età con università
        $DatiCompleti= $Dati."\n".$this->universita."\n";
        return $DatiCompleti;
    }
}
//Questo if controlla se tutti i campi del form sono stati compilati
   if ($_POST["nome"]==""||$_POST["cognome"]==""||$_POST["eta"]==""|| $_POST["universita"]==""){
       echo "Devi completare tutti i campi";
       echo "<br>"."<button onclick='javascript:history.go(-1)'>Torna ai dati da compilare</button>";
   }

   else{
       //alla variabile  $utente assegno un istanza della classe Studente()
       //così $utente diventa un oggetto di tipo classe Studente()
       //new allocazione dinamica di un'oggetto della classe Studente
        $utente  = new Studente($_POST["nome"], $_POST["cognome"], $_POST["eta"], $_POST["universita"]);
// apre il file in modalità "append", se non esiste lo crea, e con a+ legge e poi aggiunge in coda
       $fp = fopen("Utenti.txt", "a+");
// con la funzione fwrite si inserisce nel file il risultato del metodo getStudente dell'utente creato(dati completi)
//parametri dove e cosa
       fwrite($fp,$utente->getStudente());
// chiude il file
       fclose($fp);
//funzione che distugge la variabile passata
       unset($utente);
    }



